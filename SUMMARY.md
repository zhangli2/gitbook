# Summary

* [LINUX系统管理]
  * [第一章 安装虚拟机和LINUX操作系统](part1/01.md)
  * [第二章 目录和文件的基本操作](part1/02.md)
  * [第三章 安装和管理应用程序](part1/03.md)
